#include "Diccionario.hpp"

template<class T>
class Multiconjunto {
public:
    Multiconjunto();
    void agregar(T x);
    int ocurrencias(T x) const;
    bool operator<=(Multiconjunto<T> otro);
private:
    Diccionario<T, int> dicc;
};

template<class T>
Multiconjunto<T>::Multiconjunto() {}

template<class T>
void Multiconjunto<T>::agregar(T x) {
    if (dicc.def(x)) {
        dicc.definir(x, dicc.obtener(x) + 1);
    } else {
        dicc.definir(x, 1);
    }
}

template<class T>
int Multiconjunto<T>::ocurrencias(T x) const {
    if (dicc.def(x)) {
        return dicc.obtener(x);
    }
    return 0;
}

template<class T>
bool Multiconjunto<T>::operator<=(Multiconjunto<T> otro) {
    for (T k : dicc.claves()) {
        if (ocurrencias(k) > otro.ocurrencias(k)) {
            return false;
        }
    }
    return true;
}
