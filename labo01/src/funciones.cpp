#include <vector>
#include <set>
#include "algobot.h"

using namespace std;

// Ejercicio 1
vector<int> quitar_repetidos(vector<int> s) {
	vector<int> res;
	for (int i = 0; i < s.size(); i++) {
		bool repetido = false;
		for (int j = 0; j < res.size(); j++) {
			if (res[j] == s[i]) {
				repetido = true;
			}
		}
		if (!repetido) {
			res.push_back(s[i]);
		}
	}
  return res;
}

// Ejercicio 2
vector<int> quitar_repetidos_v2(vector<int> s) {
	set<int> set_res;
	for (int e: s) {
		set_res.insert(e);
	}
	vector<int> res;
	for (int e: set_res) {
		res.push_back(e);
	}
    return res;
}

// Ejercicio 3
bool mismos_elementos(vector<int> a, vector<int> b) {
	bool res = true;
	for (int i = 0; i < a.size(); i++) {
		bool encontro = false;
		for (int j = 0; j < b.size(); j++) {
			if (a[i] == b[j]) {
				encontro = true;
			}
		}
		if (!encontro) {
			res = false;
		}
	}
	for (int i = 0; i < b.size(); i++) {
		bool encontro = false;
		for (int j = 0; j < a.size(); j++) {
			if (b[i] == a[j]) {
				encontro = true;
			}
		}
		if (!encontro) {
			res = false;
		}
	}
  return res;
}

// Ejercicio 4
bool mismos_elementos_v2(vector<int> a, vector<int> b) {
  set<int> set_a, set_b;
  for (int i = 0; i < a.size(); i++) {
    set_a.insert(a[i]);
  }
  for (int i = 0; i < b.size(); i++) {
    set_b.insert(b[i]);
  }
  return set_a == set_b;
}

// Ejercicio 5
map<int, int> contar_apariciones(vector<int> s) {
  map<int, int> res;
  for (int e: s) {
    if (res.count(e) > 0) {
      res[e]++;
    } else {
      res[e] = 1;
    }
  }
  return res;
}

// Ejercicio 6
vector<int> filtrar_repetidos(vector<int> s) {
  map<int, int> apariciones = contar_apariciones(s);
  vector<int> res;
  for (int e: s) {
    if (apariciones[e] == 1) {
      res.push_back(e);
    }
  }
  return res;
}

// Ejercicio 7
set<int> interseccion(set<int> a, set<int> b) {
  set<int> res;
  for (int e: a) {
    if (b.count(e) > 0) {
      res.insert(e);
    }
  }
  return res;
}

// Ejercicio 8
map<int, set<int>> agrupar_por_unidades(vector<int> s) {
  map<int, set<int>> res;
  for (int e: s) {
    int digito = e % 10;
    res[digito].insert(e);
  }
  return res;
}

// Ejercicio 9
vector<char> traducir(vector<pair<char, char>> tr, vector<char> str) {
  vector<char> res = str;
  for (int i = 0; i < res.size(); i++) {
    bool hasntTranslated = true;
    for (int j = 0; j < tr.size() && hasntTranslated; j++) {
      if (tr[j].first == res[i]) {
        res[i] = tr[j].second;
        hasntTranslated = false;
      }
    }
  }
  return res;
}

// Ejercicio 10
bool comparteLibretas(set<LU> l1, set<LU> l2) {
  set<LU> res;
  for (LU e: l1) {
    if (l2.count(e) > 0) {
      res.insert(e);
    }
  }
  return res.size() > 0;
}
bool integrantes_repetidos(vector<Mail> s) {
  bool res = false;
  for (int i = 0; i < s.size(); i++) {
    for (int j = i + 1; j < s.size(); j++) {
      if (s[i].libretas() != s[j].libretas() && comparteLibretas(s[i].libretas(), s[j].libretas())) {
        res = true;
      }
    }
  }
  return res;
}

// Ejercicio 11
map<set<LU>, Mail> entregas_finales(vector<Mail> s) {
  map<set<LU>, Mail> res;
  for (Mail m: s) {
    if (m.adjunto() && (res.count(m.libretas()) == 0 || res[m.libretas()].fecha() < m.fecha())) {
      res[m.libretas()] = m;
    }
  }
  return res;
}
