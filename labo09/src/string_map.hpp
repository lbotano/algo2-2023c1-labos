template <typename T>
string_map<T>::string_map(){
    raiz = nullptr;
    _size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if (raiz != nullptr) {
        borrar_hijos(*raiz);
        raiz = nullptr;
    }
    _size = d._size;
    
    if (d.raiz != nullptr) {
        vector<pair<string, T>> valores;
        vector<pair<Nodo*, string>> nodos; // "stack"
        nodos.push_back(make_pair(d.raiz, ""));

        // Busco todos los valores definidos con sus claves
        while (!nodos.empty()) {
            pair<Nodo*, string> nodo_actual = nodos.back();
            nodos.pop_back();

            if (nodo_actual.first->definicion != nullptr) {
                valores.push_back(make_pair(nodo_actual.second, *nodo_actual.first->definicion));
            }

            // Agrego a los nodos siguientes al "stack"
            for (char c = 0; c < nodo_actual.first->siguientes.size(); c++) {
                Nodo* n = nodo_actual.first->siguientes[c];
                if (n != nullptr) {
                    string clave = nodo_actual.second; // Clave que accedería a este nodo
                    clave.push_back(c);
                    nodos.push_back(make_pair(n, clave));
                }
            }
        }

        for (pair<string, T> valor : valores) {
            insert(valor);
        }
    }
    return *this;
}

template <typename T>
string_map<T>::~string_map(){
    if (raiz != nullptr) {
        borrar_hijos(*raiz);
    }
}

template <typename T>
T& string_map<T>::operator[](const string& clave) {
    //
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& clave_valor) {
    Nodo* n = raiz;
    unsigned int agregar_desde = 0;

    if (raiz == nullptr) {
        // Creo el nodo raíz si éste no existe
        raiz = new Nodo();
        n = raiz;
    } else {
        // Busco desde dónde agregar los nodos de la clave
        while (
            n->siguientes[clave_valor.first[agregar_desde]] != nullptr
            && agregar_desde < clave_valor.first.size()
        ) {
            n = n->siguientes[clave_valor.first[agregar_desde]];
            agregar_desde++;
        }
    }

    // Agrego los nodos que haga falta
    if (agregar_desde < clave_valor.first.size()) {
        _size++;
    }
    while (agregar_desde < clave_valor.first.size()) {
        Nodo* nuevo_nodo = new Nodo();
        n->siguientes[clave_valor.first[agregar_desde]] = nuevo_nodo;
        n = nuevo_nodo;
        agregar_desde++;
    }

    // Pongo la definición en el último nodo
    if (n->definicion != nullptr) {
        delete n->definicion;
    }
    n->definicion = new T(clave_valor.second);
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    Nodo* n = raiz;
    if (raiz != nullptr) {
        unsigned int i = 0;
        // Busco entre los nodos hasta haber recorrido todos los caracteres de la
        // clave o haber encontrado un nodo sin el hijo necesario.
        while (i < clave.size() && n != nullptr) {
            n = n->siguientes[clave[i]];
            i++;
        }
        if (i == clave.size()) {
            if (n->definicion != nullptr) {
                return 1;
            } else {
                // La clave no está definida pero es prefijo de una que sí lo está.
                return 0;
            }
        }
    }
    return 0;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    return at(clave);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo *n = raiz;
    for (int i = 0; i < clave.size(); i++) {
        n = n->siguientes[clave[i]];
    }
    return *(n->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* n = raiz;
    Nodo* ultimo_nodo = nullptr;
    unsigned int ultimo_nodo_pos = 0;

    // Busco el nodo a borrar y el ultimo nodo sin significado o más de un hijo.
    for (int i = 0; i < clave.size(); i++) {
        n = n->siguientes[clave[i]];
        if (n->definicion != nullptr || cant_hijos(*n) > 1) {
            ultimo_nodo = n;
            ultimo_nodo_pos = i;
        }
    }
    
    if (ultimo_nodo == n) {
        // Último nodo es el mismo que el nodo a borrar.
        delete n->definicion;
        n->definicion = nullptr;
    } else {
        for (Nodo* m : ultimo_nodo->siguientes[clave[ultimo_nodo_pos + 1]]->siguientes) {
            borrar_hijos(*m);
        }
        ultimo_nodo->siguientes[clave[ultimo_nodo_pos + 1]] = nullptr;
        _size--;
    }
}

template <typename T>
int string_map<T>::size() const{
    unsigned int res = 0;
    vector<Nodo*> nodos;
    
    if (raiz != nullptr) {
        nodos.push_back(raiz);
        while (!nodos.empty()) {
            Nodo* n = nodos.back();
            nodos.pop_back();
            if (n->definicion != nullptr) {
                res++;
            }
            for (Nodo* m : n->siguientes) {
                if (m != nullptr) {
                    nodos.push_back(m);
                }
            }
        }
    }
    
    return res;
}

template <typename T>
bool string_map<T>::empty() const{
    return raiz == nullptr;
}

template <typename T>
unsigned int string_map<T>::cant_hijos(typename string_map<T>::Nodo& nodo) const {
    unsigned int res = 0;
    for (Nodo* n : nodo.siguientes) {
        if (n != nullptr) {
            res++;
        }
    }
    return res;
}

template <typename T>
void string_map<T>::borrar_hijos(typename string_map<T>::Nodo& nodo) {
    vector<Nodo*> nodos;
    nodos.push_back(&nodo);

    while(!nodos.empty()) {
        Nodo* n = nodos.back();
        nodos.pop_back();
        for (Nodo* m : n->siguientes) {
            if (m != nullptr) {
                nodos.push_back(m);
            }
        }
        delete n->definicion;
        delete n;
    }
}
