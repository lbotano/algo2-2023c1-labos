#ifndef ALGO2_LABO_CLASE5_ALGORITMOS_H
#define ALGO2_LABO_CLASE5_ALGORITMOS_H

#include <utility>
#include <iterator>
#include <vector>

using namespace std;

// template<class Contenedor>
// typename Contenedor::value_type minimo(const Contenedor& c) {
//   typename Contenedor::const_iterator it = c.begin();
//   typename Contenedor::value_type min = *it;
//   while (it != c.end()) {
//     if (*it < min) {
//       min = *it;
//     }
//     ++it;
//   }
//   return min;
// }

template<class Contenedor>
typename Contenedor::value_type promedio(const Contenedor& c) {
  typename Contenedor::const_iterator it = c.begin();
  typename Contenedor::value_type sum = 0;
  int size = 0;
  while (it != c.end()) {
    sum += *it;
    ++size;
    ++it;
  }
  return sum / size;
}

template<class Iterator>
typename Iterator::value_type minimoIter(const Iterator& desde, const Iterator& hasta) {
  Iterator it = desde;
  Iterator min = desde;
  while (it != hasta) {
    if (*it < *min) {
      min = it;
    }
    ++it;
  }
  return *min;
}


template<class Iterator>
typename Iterator::value_type promedioIter(const Iterator& desde, const Iterator& hasta) {
  Iterator it = desde;
  typename Iterator::value_type sum = 0;
  int cant = 0;
  while (it != hasta) {
    sum += *it;
    ++cant;
    ++it;
  }
  return sum / cant;
}

template<class Contenedor>
void filtrar(Contenedor &c, const typename Contenedor::value_type& elem) {
  typename Contenedor::iterator it = c.begin();
  while (it != c.end()) {
    if (*it == elem) {
      // Es muy importante pasar al siguiente iterador antes de eliminar el actual
      // El orden en el que se ejecuta esta línea es el siguiente:
      //   1. Se le pasa el valor de it a erase
      //   2. Se incrementa it
      //   3. Se ejecuta erase con el valor ya pasado
      c.erase(it++);
    } else {
      ++it;
    }
  }
}

template<class Contenedor>
bool ordenado(Contenedor &c) {
  typename Contenedor::const_iterator it = c.begin();
  typename Contenedor::value_type anterior = *it;
  it++;
  while (it != c.end()) {
    if (anterior > *it) {
      return false;
    }
    anterior = *it;
    ++it;
  }
  return true;
}

template<class Contenedor>
std::pair<Contenedor, Contenedor> split(const Contenedor& c,
                                        const typename Contenedor::value_type& elem) {
  typename Contenedor::const_iterator it = c.begin();
  std::pair<Contenedor, Contenedor> res;
  while (it != c.end()) {
    if (*it < elem) {
      res.first.insert(res.first.end(), *it);
    }
    if (*it >= elem) {
      res.second.insert(res.second.end(), *it);
    }
    ++it;
  }
  return res;
}

template<class Contenedor>
void merge(const Contenedor& c1, const Contenedor& c2, Contenedor& res) {
  typename Contenedor::const_iterator itC1 = c1.begin();
  typename Contenedor::const_iterator itC2 = c2.begin();
  while (itC1 != c1.end() || itC2 != c2.end()) {
    if (itC1 != c1.end() && itC2 != c2.end() && *itC1 < *itC2 || itC2 == c2.end()) {
      res.insert(res.end(), *itC1);
      ++itC1;
    } else if (itC1 != c1.end() && itC2 != c2.end() && *itC1 >= *itC2 || itC1 == c1.end()){
      res.insert(res.end(), *itC2);
      ++itC2;
    }
  }
}

template<class Contenedor>
typename Contenedor::value_type minimo(const Contenedor& c) {
  typename Contenedor::const_iterator it = c.begin();
  typename Contenedor::const_iterator min = it;
  while (it != c.end()) {
    if (*it < *min) {
      min = it;
    }
    ++it;
  }
  return *min;
}

#endif //ALGO2_LABO_CLASE5_ALGORITMOS_H
