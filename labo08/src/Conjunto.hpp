template<class T>
Conjunto<T>::Nodo::Nodo(const T& v) {
    valor = v;
    izq = nullptr;
    der = nullptr;
}

template <class T>
Conjunto<T>::Conjunto() {
    _raiz = nullptr;
    _cardinal = 0;
}

template <class T>
Conjunto<T>::~Conjunto() { 
    while (_raiz != nullptr) {
        remover(_raiz->valor);
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* n = _raiz;
    while (n != nullptr) {
        if (n->valor == clave) {
            return true;
        } if (clave <= n->valor) {
            n = n->izq;
        } else {
            n = n->der;
        }
    }
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo* n = _raiz;
    Nodo* padre = nullptr;
    while (n != nullptr) {
        padre = n;
        if (clave == n->valor) {
            return; // No hace falta hacer nada.
        } else if (clave < n->valor) {
            n = n->izq;
        } else {
            n = n->der;
        }
    }
    if (padre == nullptr) {
        _raiz = new Nodo(clave);
    } else if (clave < padre->valor) {
        padre->izq = new Nodo(clave);
    } else {
        padre->der = new Nodo(clave);
    }
    _cardinal++;
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (pertenece(clave)) {
        Nodo* aBorrar = _raiz;      // Nodo a borrar
        Nodo* padre = nullptr;      // Padre del nodo a borrar, nullptr si es la raíz.
        Nodo* reemplazo = nullptr;  // El nodo que va a ocupar el lugar del que será borrado.
        bool esHijoIzq = false;

        // Busco el nodo a borrar
        while (aBorrar->valor != clave) {
            padre = aBorrar;
            if (clave < aBorrar->valor) {
                esHijoIzq = true;
                aBorrar = aBorrar->izq;
            } else {
                esHijoIzq = false;
                aBorrar = aBorrar->der;
            }
        }

        if (aBorrar->izq != nullptr && aBorrar->der != nullptr) { // Dos hijos
            reemplazo = aBorrar->izq;
            Nodo* padreReemplazo = nullptr;
            while (reemplazo->der != nullptr) {
                padreReemplazo = reemplazo;
                reemplazo = reemplazo->der;
            }

            if (padreReemplazo != nullptr) {
                padreReemplazo->der = reemplazo->izq;
                reemplazo->izq = aBorrar->izq;
            }
            reemplazo->der = aBorrar->der;
        } else if (aBorrar->izq != nullptr || aBorrar->der != nullptr) { // Un hijo
            if (aBorrar->izq != nullptr) {
                reemplazo = aBorrar->izq;
            } else {
                reemplazo = aBorrar->der;
            }
        }

        if (padre == nullptr) {
            _raiz = reemplazo;
        } else {
            if (esHijoIzq) {
                padre->izq = reemplazo;
            } else {
                padre->der = reemplazo;
            }
        }
        delete aBorrar;
        _cardinal--;
    }
}


template <class T>
const T& Conjunto<T>::siguiente(const T& elem) {
    return _nodoSiguiente(elem)->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    return _nodoMinimo()->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* n = _raiz;
    while (n->der != nullptr) {
        n = n->der;
    }
    return n->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream& os) const {
    Nodo* n = _nodoMinimo();
    os << "<";
    if (n != nullptr) {
        os << n->valor;
        n = _nodoSiguiente(n->valor);
    }
    while (n != nullptr) {
        os << ", " << n->valor;
        n = _nodoSiguiente(n->valor);
    }
    os << ">";
}

template <class T>
typename Conjunto<T>::Nodo* Conjunto<T>::_nodoMinimo() const {
    Nodo* n = _raiz;
    while (n != nullptr) {
        if (n->izq == nullptr) {
            return n;
        }
        n = n->izq;
    }
    return n;
}

template <class T>
typename Conjunto<T>::Nodo* Conjunto<T>::_nodoSiguiente(const T& clave) const {
    Nodo* n = _raiz;
    Nodo* padre = nullptr;
    bool esHijoIzq = false;
    while (n->valor != clave && n != nullptr) {
        padre = n;
        if (clave < n->valor) {
            esHijoIzq = true;
            n = n->izq;
        } else {
            esHijoIzq = false;
            n = n->der;
        }
    }
    if (n == nullptr || !esHijoIzq && n->der == nullptr) {
        return nullptr;
    } else if (n->der == nullptr) {
        return padre;
    }
    // Busco el valor más cercano a clave por derecha
    n = n->der;
    while (n->izq != nullptr) {
        n = n->izq;
    }
    return n;
}
