#include <utility>
#include <iostream>
#include <vector>

using namespace std;

// Taller 1 - Juego

using Pos = pair<int, int>;

char ARRIBA = '^';
char ABAJO = 'v'; 
char DERECHA = '<';
char IZQUIERDA = '>';

class Pocion {
    public:
        Pocion(uint turno_inicial, uint movimientos, uint duracion);
        uint turno_inicial();
        uint movimientos();
        uint ultimo_turno(); // Excluyente
    private:
        uint turno_inicial_;
        uint movimientos_;
        uint ultimo_turno_;
};

Pocion::Pocion(uint turno_inicial, uint movimientos, uint duracion) {
    // Precalculamos en qué turno termina cada poción
    turno_inicial_ = turno_inicial;
    movimientos_ = movimientos;
    ultimo_turno_ = turno_inicial + duracion;
}

uint Pocion::turno_inicial() {
    return turno_inicial_;
}

uint Pocion::movimientos() {
    return movimientos_;
}

uint Pocion::ultimo_turno() {
    return ultimo_turno_;
}

class Juego {
    public:
        Juego(uint casilleros, Pos posicion_inicial);
        Pos posicion_jugador();
        uint turno_actual();
        void mover_jugador(char dir);
        void ingerir_pocion(uint movimientos, uint turnos);
    private:
        vector<Pocion> pociones_;
        uint casilleros_;
        Pos posicion_jugador_;
        uint turno_actual_;
        uint movimientos_este_turno_();
        uint movimientos_hechos_;
        void jugador_movido_(); // Se llama después de que se haya movido el jugador
};

Juego::Juego(uint casilleros, Pos posicion_inicial) {
    casilleros_ = casilleros;
    posicion_jugador_ = posicion_inicial;
    turno_actual_ = 0;
    movimientos_hechos_ = 0;
}

Pos Juego::posicion_jugador() {
    return posicion_jugador_;
}

uint Juego::turno_actual() {
    return turno_actual_;
}

void Juego::mover_jugador(char dir) {
    // Verifica que no se salga del mapa
    if (dir == ARRIBA && posicion_jugador().first > 0) {
        posicion_jugador_.first--;
        jugador_movido_();
    } else if (dir == ABAJO && posicion_jugador().first < casilleros_ - 1) {
        posicion_jugador_.first++;
        jugador_movido_();
    } else if (dir == IZQUIERDA && posicion_jugador().second > 0) {
        posicion_jugador_.second--;
        jugador_movido_();
    } else if (dir == DERECHA && posicion_jugador().second < casilleros_ - 1) {
        posicion_jugador_.second++;
        jugador_movido_();
    }  
}

void Juego::ingerir_pocion(uint movimientos, uint turnos) {
    Pocion p = Pocion(turno_actual(), movimientos, turnos);
    pociones_.push_back(p);
}

uint Juego::movimientos_este_turno_() {
    uint res = 0;
    for (Pocion p: pociones_) {
        if (p.turno_inicial() <= turno_actual() && turno_actual() < p.ultimo_turno()) {
            res += p.movimientos();
        }
    }

    // Si no hay pociones en efecto, hay un movimiento.
    if (res > 0) {
        return res;
    }
    return 1;
}

void Juego::jugador_movido_() {
    movimientos_hechos_++;
    if (movimientos_hechos_ >= movimientos_este_turno_()) {
        // Pasar turno
        turno_actual_++;
        movimientos_hechos_ = 0;
    }
}
