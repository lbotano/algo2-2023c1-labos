#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    void incrementar_dia();

  private:
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia) {
    mes_ = mes;
    dia_ = dia;
}

int Fecha::mes() {
    return mes_;
}

int Fecha::dia() {
    return dia_;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}


#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    // Completar iguadad (ej 9)
    return igual_dia && igual_mes;
}
#endif

void Fecha::incrementar_dia() {
    dia_++;
    if (dia() > dias_en_mes(mes())) {
        dia_ = 1;
        mes_++;
    }
}

// Ejercicio 11, 12
class Horario {
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario otro);
        bool operator<(Horario otro);
    private:
        uint hora_;
        uint min_;
};

Horario::Horario(uint hora, uint min) {
    hora_ = hora;
    min_ = min;
}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario otro) {
    bool misma_hora = hora() == otro.hora();
    bool mismo_min  = min()  == otro.min();
    return misma_hora && mismo_min;
}

bool Horario::operator<(Horario otro) {
    if (hora() == otro.hora()) {
        return min() < otro.min();
    }
    return hora() < otro.hora();
}

// Ejercicio 13

class Recordatorio {
    public:
        Recordatorio(Fecha fecha, Horario horario, string mensaje);
        string mensaje();
        Fecha fecha();
        Horario horario();
        bool operator<(Recordatorio otro);
    private:
        string mensaje_;
        Fecha fecha_;
        Horario horario_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje) : fecha_(fecha), horario_(horario), mensaje_(mensaje) {}

string Recordatorio::mensaje() {
    return mensaje_;
}

Fecha Recordatorio::fecha() {
    return fecha_;
}

Horario Recordatorio::horario() {
    return horario_;
}

bool Recordatorio::operator<(Recordatorio otro) {
    return this->horario() < otro.horario();
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}


// Ejercicio 14

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();
    private:
        list<Recordatorio> recordatorios_;
        Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial) {}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_.push_back(rec);
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> res;

    // Filtrar recordatorios de hoy
    for (Recordatorio rec: recordatorios_) {
        if (rec.fecha() == hoy()) {
            res.push_back(rec);
        }
    }

    res.sort();

    return res;
}

Fecha Agenda::hoy() {
    return hoy_;
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << std::endl;
    os << "=====" << std::endl;
    for (Recordatorio r: a.recordatorios_de_hoy()) {
        os << r << std::endl;
    }
    return os;
}
