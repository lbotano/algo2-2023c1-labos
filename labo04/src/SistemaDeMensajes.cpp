#include "SistemaDeMensajes.h"

SistemaDeMensajes::SistemaDeMensajes() {
  // Inicializo el array con nullptr para poder saber después si cada jugador
  // está registrado.
  for (int i = 0; i < 4; i++) {
    _conns[i] = nullptr;
  }
}

SistemaDeMensajes::~SistemaDeMensajes() {
  for (int i = 0; i < 4; i++) {
    delete _conns[i];
    _conns[i] = nullptr;
  }
  for (Proxy* p : _proxies) {
    delete p;
  }
}

void SistemaDeMensajes::registrarJugador(int id, string ip) {
  // Si el jugador ya existe, primero tengo que eliminarlo de la memoria.
  if (registrado(id)) {
    desregistrarJugador(id);
  }
  // Uso new para que quede guardado en el heap.
  _conns[id] = new ConexionJugador(ip);
}

void SistemaDeMensajes::desregistrarJugador(int id) {
  // Primero aviso que voy a dejar de usar la dirección de memoria y luego
  // dejo de usarla.
  delete _conns[id];
  _conns[id] = nullptr;
}

void SistemaDeMensajes::enviarMensaje(int id, string mensaje) {
  // Uso -> porque _conns guarda punteros.
  _conns[id]->enviarMensaje(mensaje);
}

bool SistemaDeMensajes::registrado(int id) const {
  return _conns[id] != nullptr;
}

string SistemaDeMensajes::ipJugador(int id) const {
  return _conns[id]->ip();
}

SistemaDeMensajes::Proxy* SistemaDeMensajes::obtenerProxy(int id) {
  Proxy* proxy = new Proxy(&_conns[id]);
  _proxies.push_back(proxy);
  return proxy;
}

SistemaDeMensajes::Proxy::Proxy(ConexionJugador** conn) : _conn(conn) {}

void SistemaDeMensajes::Proxy::enviarMensaje(string msg) {
  (*_conn)->enviarMensaje(msg);
}
