#include "gtest-1.8.1/gtest.h"
#include "../src/Libreta.h"
#include "../src/Truco.h"
#include <map>
#include <cmath>

using namespace std;

// Ejercicio 4
TEST(Aritmetica, suma) {
  int suma = 15 + 7;
  int expected = 22;
  EXPECT_EQ(suma, expected);
}

// Ejercicio 5
TEST(Aritmetica, potencia) {
  int potencia = pow(10, 2);
  int expected = 100;
  EXPECT_EQ(potencia, expected);
}

// Ejercicios 6..9
TEST(Aritmetica, potencia_general) {
  for (int i = -5; i <= 5; i++) {
    int potencia = pow(i, 2);
    int expected = i * i;
    EXPECT_EQ(potencia, expected);
  }
}

TEST(Diccionario, obtener) {
  map<int, int> diccionario;
  diccionario[23] = 432;
  EXPECT_EQ(diccionario[23], 432);
}

TEST(Diccionario, definir) {
  map<int, int> diccionario;
  EXPECT_EQ(diccionario.count(23), 0);
  diccionario[23] = 13;
  EXPECT_EQ(diccionario.count(23), 1);
}

TEST(Truco, inicio) {
  Truco t;
  EXPECT_EQ(t.puntaje_j1(), 0);
  EXPECT_EQ(t.puntaje_j2(), 0);
}

TEST(Truco, buenas) {
  Truco t;
  EXPECT_FALSE(t.buenas(1));
  for (int i = 0; i < 15; i++) {
    t.sumar_punto(1);
  }
  EXPECT_FALSE(t.buenas(1));
  t.sumar_punto(1);
  EXPECT_TRUE(t.buenas(1));
  t.sumar_punto(1);
  t.sumar_punto(1);
  EXPECT_TRUE(t.buenas(1));
}
