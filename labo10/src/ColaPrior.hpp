#include "ColaPrior.h"

template<class T>
ColaPrior<T>::ColaPrior() {
	// COMPLETAR
}

template<class T>
int ColaPrior<T>::tam() const {
	return _cola.size();
}

template<class T>
void ColaPrior<T>::encolar(const T& elem) {
	_cola.push_back(elem);
	int i = _cola.size() - 1;
	while (i > 0) {
		const int padreIzq = (i - 1) / 2; // Posición del padre de un hijo izquierdo
		const int padreDer = (i - 2) / 2; // Posición del padre de un hijo derecho
		if (i % 2 == 1 && _cola[padreIzq] < _cola[i]) {
			T tmp = _cola[i];
			_cola[i] = _cola[padreIzq];
			_cola[padreIzq] = tmp;
			i = padreIzq;
		} else if (i % 2 == 0 && _cola[padreDer] < _cola[i]) {
			T tmp = _cola[i];
			_cola[i] = _cola[padreDer];
			_cola[padreDer] = tmp;
			i = padreDer;
		} else {
			i = 0;
		}
	}
}

template<class T>
const T& ColaPrior<T>::proximo() const {
	return _cola.front();
}

template<class T>
void ColaPrior<T>::desencolar() {
	const T ultimo = _cola.back();
	_cola.pop_back();
	_cola[0] = ultimo;
	int i = 0;
	while (i < _cola.size()) {
		unsigned int suma = 1;
		if (2 * i + 2 < _cola.size() && _cola[i] < _cola[2 * i + 2] && _cola[2 * i + 1] < _cola[2 * i + 2]) {
			// Swapear con hijo derecho
			T tmp = _cola[i];
			_cola[i] = _cola[2 * i + 2];
			_cola[2 * i + 2] = tmp;
			suma = 2;
		} else if (2 * i + 1 < _cola.size() && _cola[i] < _cola[2 * i + 1]) {
			// Swapear con hijo izquierdo
			T tmp = _cola[i];
			_cola[i] = _cola[2 * i + 1];
			_cola[2 * i + 1] = tmp;
		}
		i = 2 * i + suma;
	}
}

template<class T>
ColaPrior<T>::ColaPrior(const vector<T>& elems) {
	// COMPLETAR
	for (const T& e: elems) {
		encolar(e);
	}
}

