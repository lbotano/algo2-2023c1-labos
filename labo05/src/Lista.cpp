#include "Lista.h"
#include <cassert>

Lista::Lista() : prim(nullptr), ult(nullptr) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    destruirNodos();
}

Lista& Lista::operator=(const Lista& aCopiar) {
    destruirNodos();
    copiarNodos(aCopiar);
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nuevoNodo = new Nodo(elem, prim, nullptr);
    if (prim != nullptr) {
        prim->prev = nuevoNodo;
    } else {
        ult = nuevoNodo;
    }
    prim = nuevoNodo;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nuevoNodo = new Nodo(elem, nullptr, ult);
    if (ult != nullptr) {
        ult->sig = nuevoNodo;
    } else {
        prim = nuevoNodo;
    }
    ult = nuevoNodo;
}

void Lista::eliminar(Nat i) {
    if (i < longitud()) {
        Nodo* aEliminar = prim;
        for (int j = 0; j < i; j++) {
            aEliminar = aEliminar->sig;
        }

        if (aEliminar->prev == nullptr) {
            prim = aEliminar->sig;
        } else {
            aEliminar->prev->sig = aEliminar->sig;
        }

        if (aEliminar->sig == nullptr) {
            ult = aEliminar->prev;
        } else {
            aEliminar->sig->prev = aEliminar->prev;
        }

        delete aEliminar;
    }
}

int Lista::longitud() const {
    int res = 0;
    Nodo* n = prim;
    while (n != nullptr) {
        res++;
        n = n->sig;
    }
    return res;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* res = prim;
    for (int j = 0; j < i; j++) {
        res = res->sig;
    }
    return res->valor;
}

int& Lista::iesimo(Nat i) {
    Nodo* res = prim;
    for (int j = 0; j < i; j++) {
        res = res->sig;
    }
    return res->valor;
}

void Lista::mostrar(ostream& o) {
    o << "< ";
    Nodo* n = prim;
    while (n != nullptr) {
        o << n->valor << ", ";
        n = n->sig;
    }
    o << ">";
}

void Lista::destruirNodos() {
    Nodo* n = prim;
    while (n != nullptr) {
        Nodo* sig = n->sig;
        delete n;
        n = sig;
    }
    prim = nullptr;
    ult = nullptr;
}

void Lista::copiarNodos(const Lista& aCopiar) {
    Nodo* n = aCopiar.prim;
    while (n != nullptr) {
        agregarAtras(n->valor);
        n = n->sig;
    }
}
